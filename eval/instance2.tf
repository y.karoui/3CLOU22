#resource "scaleway_instance_ip" "public_ip1" {
#  project_id = var.project_id
#}

#resource "scaleway_instance_ip" "public_ip2" {
#  project_id = var.project_id
#}


resource "scaleway_instance_server" "web3" {
  name = "${local.team}-web3"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]
  
  user_data = {
    name        = "web3"
    myip        = "10.42.42.13"
    cloud-init = file("${path.module}/init_instance.sh")
    #cloud-init = file("${path.module}/deploy-wp")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }

  #depends_on =  [ scaleway_instance_server.web1 ]
  #ip_id = scaleway_instance_ip.public_ip2.id

  #additional_volume_ids = [scaleway_instance_volume.data.id]

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  security_group_id = scaleway_instance_security_group.sg-www.id
}

resource scaleway_instance_private_nic "nic3" {
  private_network_id = scaleway_vpc_private_network.myvpc.id
  server_id          = scaleway_instance_server.web3.id
}

#output "web3_ip" {
#  value = "${scaleway_instance_server.web3.public_ip}"
#}

